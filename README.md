### Using protobuf-v3
Protobuf v3 comes with pretty nice definition that normally used for GRPC, including 
- Solid data types
- Quite known pre-defined format
- Service definition ready


### Current state
- [x] Define structure
- [x] Code skeleton for protobuf generator
- [X] Define golang implementation
- [X] Define typescript implementation
- [X] Tests
- [ ] Incorporate tracing and monitoring into the implementations

### Project structure
    .
    ├── proto                  # example used to test this tool
    ├── protoc-gen-goff        # a protoc plugin it's responsible for generating source code from .proto file
    ├── protoc-gen-parser      # a protoc plugin it's responsible for compiling .proto file to intermediate binary file this is used by protoc-gen-goff in debuging 
    ├── generate.sh            # command compiles 'proto' example above by using protoc-gen-goff
    ├── goff.proto             # protobuf extension defination
    ├── parser.sh              # command compiles 'proto' example above by using protoc-gen-parser
    ├── LICENSE
    └── README.md


### How to run example
- Go installed
- Make sure probuffer installed (can install using `brew install protobuf`)
- Confirm the installation by having protoc in code
- Execute generate.sh from root dir (can also use go generate todo ...)

## Noted (naming proto file is matter)
- We decided only gen action methods and their listener when the proto file contant `app` or
  `bff`. The reason behind it is we encounrage every one (especial FE code) to access the service via 
  `bff` not call service dirrectly. 


### How to use current repo
check the link below
```
https://www.notion.so/silenteer/RFD-Simplify-even-further-BFF-integration-using-code-generation-d50823d79d57430685cd855edba9be54
```

### Option Examples:  
- **[(goff.custom) = '<value>']**
  - **validate**: from  https://github.com/go-playground/validator.  
    - Example: [(goff.custom) = 'validate:"notblank"'], [(goff.custom) = 'validate:"required,oneof=MEMBER PATIENT ORGANIZATION"']   
  - **customize struct tags:**  
    - Example: [(goff.custom) = 'json:"omitempty"'];  
- **[(goff.nullable) = true]** //Set basic type to pointer.   
- **goff.Empty**
  -   rpc getAllDevices (goff.Empty) returns (AdminDevicesDto) =>func getAllDevices() (AdminDevicesDto, error)
  -   rpc getAllDevices (Model) returns (goff.Empty) =>func getAllDevices(Model) (error)
- **Mix options:**   [(goff.custom) = 'json:"gender,omitempty"', (goff.nullable) = true]; 
 
 
 ### Env parameters
 - TS_SOCKET_PATH: 
     - Change default import socket path on generated ts files:
     - Default value: @tutum/hermes/websocket