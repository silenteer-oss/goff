clear

rm -rf ./out/todo.bff.data
mkdir -p ./out

go install ./protoc-gen-parser
protoc --proto_path=./ --proto_path=./proto --parser_out ./out ./proto/todo.bff.proto