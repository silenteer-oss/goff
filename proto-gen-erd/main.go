package main

import (
	_ "embed"
	"flag"
	"fmt"
	"log"
	"os"
)

//go:embed template.go.tmpl
var goTemplateFile string

func main() {
	log.SetPrefix("protoc-gen-erd: ")

	var (
		repoDir          string
		configOutputFile string
	)
	flag.StringVar(&configOutputFile, "o", "", "Please specify where to output Graphviz dot file")

	flag.Parse()

	if flag.NArg() < 1 {
		log.Fatal("Missing repository directory")
		os.Exit(1)
	}

	repoDir = flag.Arg(0)

	fmt.Println("parsing repo file: ", repoDir)

	databases, err := ParseProtoFiles(repoDir)

	if err != nil {
		log.Fatal(err)
		return
	}

	// now generate template
	config := DbConfig{
		Data: databases,
	}

	GenerateCode(goTemplateFile, config, configOutputFile)

	log.Println("Done.")
}
