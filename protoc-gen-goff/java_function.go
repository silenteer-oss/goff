package main

import (
	"path"
	"strings"

	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	"github.com/iancoleman/strcase"
	"gitlab.com/silenteer-oss/goff/protoc-gen-goff/generator"
)

func javaFileName(d *descriptor.FileDescriptorProto) string {
	_, impPath := g.GetGoPackageName(d)
	pkg := strcase.ToCamel(*d.Package)
	name := path.Join(impPath, pkg+"Api.java")
	return name
}

func javaType(field *descriptor.FieldDescriptorProto) string {
	// please dont ask me why
	if field.TypeName != nil {
		if strings.Compare(".goff.UUID", *field.TypeName) == 0 {
			if isRepeated(field) {
				return "List<UUID>"
			}
			return "UUID"
		}

		if strings.Compare(".goff.HttpResponse", *field.TypeName) == 0 {
			if isRepeated(field) {
				return "List<HttpResponse>"
			}
			return "HttpResponse"
		}

		if strings.Compare(".goff.UserInfo", *field.TypeName) == 0 {
			if isRepeated(field) {
				return "List<UserInfo>"
			}
			return "UserInfo"
		}

		if strings.Compare(".goff.HttpHeader", *field.TypeName) == 0 {
			return "Map<String, String>"
		}

		if strings.Compare(".goff.Time", *field.TypeName) == 0 {
			if isRepeated(field) {
				return "List<Long>"
			}
			return "Long"
		}

	}

	repeated := isRepeated(field)

	typ := ""

	switch *field.Type {
	case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
		typ = "double"
	case descriptor.FieldDescriptorProto_TYPE_FLOAT:
		typ = "float"
	case descriptor.FieldDescriptorProto_TYPE_INT64:
		typ = "long"
	case descriptor.FieldDescriptorProto_TYPE_UINT64:
		typ = "long"
	case descriptor.FieldDescriptorProto_TYPE_INT32:
		typ = "int"
	case descriptor.FieldDescriptorProto_TYPE_UINT32:
		typ = "long"
	case descriptor.FieldDescriptorProto_TYPE_FIXED64:
		typ = "long"
	case descriptor.FieldDescriptorProto_TYPE_FIXED32:
		typ = "int"
	case descriptor.FieldDescriptorProto_TYPE_BOOL:
		typ = "boolean"
	case descriptor.FieldDescriptorProto_TYPE_STRING:
		typ = "String"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED32:
		typ = "int"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED64:
		typ = "long"
	case descriptor.FieldDescriptorProto_TYPE_SINT32:
		typ = "int"
	case descriptor.FieldDescriptorProto_TYPE_SINT64:
		typ = "long"
	case descriptor.FieldDescriptorProto_TYPE_BYTES:
		typ = "byte[]"
	case descriptor.FieldDescriptorProto_TYPE_GROUP, descriptor.FieldDescriptorProto_TYPE_MESSAGE:
		desc := g.ObjectNamed(field.GetTypeName())
		descriptor, ok := desc.(*generator.Descriptor)
		if ok && len(descriptor.Field) == 2 && len(descriptor.TypeName()) == 2 {
			repeated = false
			typ = "Map<" + upperCaseType(javaType(descriptor.Field[0])) + "," + upperCaseType(javaType(descriptor.Field[1])) + ">"
		} else {
			pk := g.JavaPackageName(desc)
			if pk != "" && strings.Contains(pk, ".") {
				typ = uppercaseFirst(strings.Replace(pk, ".", "", 1)) + "Api." + uppercaseFirst(strings.Join(desc.TypeName(), "_"))
			} else {
				typ = g.TypeName(desc)
			}
		}
	case descriptor.FieldDescriptorProto_TYPE_ENUM:
		desc := g.ObjectNamed(field.GetTypeName())
		pk := g.JavaPackageName(desc)
		if pk != "" && strings.Contains(pk, ".") {
			typ = uppercaseFirst(strings.Replace(pk, ".", "", 1)) + "Api." + uppercaseFirst(strings.Join(desc.TypeName(), "_"))
		} else {
			typ = g.TypeName(desc)
		}
	default:
		g.Fail("unknown type for", field.GetName())
	}

	if isNullable(field) || repeated {
		if typ == "int" {
			typ = "Integer"
		}
		if !strings.HasSuffix(typ, "[]") {
			typ = uppercaseFirst(typ)
		}
	}

	if repeated {
		typ = "List<" + typ + ">"
	}

	return typ

}

func javaTypeString(typName string) string {
	if strings.Compare(".goff.Empty", typName) == 0 {
		return ""
	}
	if strings.Compare(".goff.HttpResponse", typName) == 0 {
		return "HttpResponse"
	}
	if strings.Compare(".goff.UserInfo", typName) == 0 {
		return "UserInfo"
	}
	if strings.Compare(".goff.HttpHeader", typName) == 0 {
		return "HttpHeaders"
	}

	var typ string
	desc := g.ObjectNamed(typName)
	_, ok := desc.(*generator.Descriptor)
	if ok {
		pk := g.JavaPackageName(desc)
		if pk != "" && strings.Contains(pk, ".") {
			typ = uppercaseFirst(strings.Replace(pk, ".", "", 1)) + "Api." + uppercaseFirst(strings.Join(desc.TypeName(), "_"))
		} else {
			typ = g.TypeName(desc)
		}
	} else {
		typ := g.TypeName(desc)
		if strings.Contains(typ, ".") {
			return typ[strings.LastIndex(typ, ".")+1:]
		}
	}
	return typ
}

func upperCaseType(typ string) string {
	if typ == "int" {
		return "Integer"
	}

	return uppercaseFirst(typ)
}

func getJavaMethodSecured(f *descriptor.MethodDescriptorProto) string {
	secured := getMethodSecured(f)
	if secured == "" {
		return ""
	}
	var rules []string
	for _, opt := range strings.Split(secured, ",") {
		opt := strings.Trim(opt, " ")
		if strings.EqualFold("IS_AUTHENTICATED", opt) {
			rules = append(rules, "SecurityRule.IS_AUTHENTICATED")
		} else if strings.EqualFold("IS_ANONYMOUS", opt) {
			rules = append(rules, "SecurityRule.IS_ANONYMOUS")
		} else if strings.EqualFold("DENY_ALL", opt) {
			rules = append(rules, "SecurityRule.DENY_ALL")
		} else {
			rules = append(rules, "Constants."+strings.ToUpper(opt))
		}
	}

	return "@Secured({" + strings.Join(rules, ",") + "})"
}
