package main

import (
	"os"
	"path"
	"strings"

	"github.com/fatih/structtag"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/protoc-gen-go/descriptor"
	"gitlab.com/silenteer-oss/goff"
	"gitlab.com/silenteer-oss/goff/protoc-gen-goff/generator"
)

// tsFileName returns the output name for the generated Go file.
func tsFileName(d *descriptor.FileDescriptorProto) (name string) {
	tsOption := getTsFileOption(d)
	pkg, impPath := g.GetGoPackageName(d)
	if protocPackage := g.GetProtoPackage(d); protocPackage != "" && useProtoPackageForTsFile {
		name = path.Join(impPath, protocPackage+".ts")
	} else if tsOption != "" {
		name = path.Join(impPath, tsOption+".ts")
	} else {
		name = path.Join(impPath, pkg+".ts")
	}
	return name
}

func tsLegacyFileName(d *descriptor.FileDescriptorProto) string {
	tsOption := getTsFileOption(d)
	pkg, impPath := g.GetGoPackageName(d)
	name := ""
	if protocPackage := g.GetProtoPackage(d); protocPackage != "" && useProtoPackageForTsFile {
		name = path.Join(impPath, "legacy", protocPackage+".ts")
	} else if tsOption != "" {
		name = path.Join(impPath, "legacy", tsOption+".ts")
	} else {
		name = path.Join(impPath, "legacy", pkg+".ts")
	}
	return name
}

func tsMobileFileName(d *descriptor.FileDescriptorProto) string {
	tsOption := getTsFileOption(d)
	pkg, impPath := g.GetGoPackageName(d)
	name := ""
	if protocPackage := g.GetProtoPackage(d); protocPackage != "" && useProtoPackageForTsFile {
		name = path.Join(impPath, "mobile", protocPackage+".ts")
	} else if tsOption != "" {
		name = path.Join(impPath, "mobile", tsOption+".ts")
	} else {
		name = path.Join(impPath, "mobile", pkg+".ts")
	}
	return name
}

func tsLoadTestFileName(d *descriptor.FileDescriptorProto) string {
	tsOption := getTsFileOption(d)
	pkg, impPath := g.GetGoPackageName(d)
	name := ""
	if protocPackage := g.GetProtoPackage(d); protocPackage != "" && useProtoPackageForTsFile {
		name = path.Join(impPath, "k6", protocPackage+".ts")
	} else if tsOption != "" {
		name = path.Join(impPath, "k6", tsOption+".ts")
	} else {
		name = path.Join(impPath, "k6", pkg+".ts")
	}
	return name
}

func getTsFileOption(f *descriptor.FileDescriptorProto) string {
	if opts := f.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_TsFile); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}

func tsType(field *descriptor.FieldDescriptorProto) string {
	// please dont ask me why
	if field.TypeName != nil {
		if strings.Compare(".goff.UUID", *field.TypeName) == 0 {
			if isRepeated(field) {
				return "Array<string>"
			}
			return "string"
		}

		if strings.Compare(".goff.HttpResponse", *field.TypeName) == 0 {
			if isRepeated(field) {
				return "Array<object>"
			}
			return "object"
		}

		if strings.Compare(".goff.HttpHeader", *field.TypeName) == 0 {
			return "{ [key: string]: string | string[] }"
		}

		if strings.Compare(".goff.Time", *field.TypeName) == 0 {
			if isRepeated(field) {
				return "Array<Date>"
			}
			return "Date"
		}
	}
	repeated := isRepeated(field)
	typ := ""

	switch *field.Type {
	case descriptor.FieldDescriptorProto_TYPE_DOUBLE:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_FLOAT:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_INT64:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_UINT64:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_INT32:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_UINT32:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_FIXED64:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_FIXED32:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_BOOL:
		typ = "boolean"
	case descriptor.FieldDescriptorProto_TYPE_STRING:
		typ = "string"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED32:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_SFIXED64:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_SINT32:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_SINT64:
		typ = "number"
	case descriptor.FieldDescriptorProto_TYPE_BYTES:
		typ = "Int8Array"
	case descriptor.FieldDescriptorProto_TYPE_GROUP, descriptor.FieldDescriptorProto_TYPE_MESSAGE, descriptor.FieldDescriptorProto_TYPE_ENUM:
		desc := g.ObjectNamed(field.GetTypeName())
		descriptor, ok := desc.(*generator.Descriptor)
		if ok && len(descriptor.Field) == 2 && len(descriptor.TypeName()) == 2 {
			repeated = false
			typ = "{[key:" + tsType(descriptor.Field[0]) + "]:" + tsType(descriptor.Field[1]) + "}"
		} else {
			typ = g.TypeName(desc)
		}
	default:
		g.Fail("unknown type for", field.GetName())
	}
	if repeated {
		typ = "Array<" + typ + ">"
	}

	return typ

}

// getTsImports return [filepath]alias
func getTsImports(f *descriptor.FileDescriptorProto) map[string]string {
	imports := make(map[string]string)
	if useProtoPackageForTsFile {
		imports = g.GetTsImportByProtoPackage(f)
	} else {
		imports = g.GetTsImports(f)
	}
	for path, pkg := range imports {
		if pkg == "" {
			lstIndex := strings.LastIndex(path, "/")
			imports[path] = path[lstIndex+1:]
		}
	}

	delete(imports, "gitlab.com/silenteer-oss/goff")
	delete(imports, "goff")
	return imports
}

func tsPropertyName(field *descriptor.FieldDescriptorProto) string {
	if propertyName := field.Name; propertyName != nil {
		name := strings.ToLower((*propertyName)[0:1]) + (*propertyName)[1:]
		if isNullable(field) {
			name = name + "?"
		}
		return name
	}
	return ""
}

func tsTypeS(typName string) string {
	if strings.Compare(".goff.Empty", typName) == 0 {
		return ""
	}

	if strings.Compare(".goff.HttpResponse", typName) == 0 {
		return "object"
	}

	if strings.Compare(".goff.UserInfo", typName) == 0 {
		return "object"
	}

	if strings.Compare(".goff.HttpHeader", typName) == 0 {
		return "{ [key: string]: string | string[] }"
	}

	desc := g.ObjectNamed(typName)
	typ := g.TypeName(desc)
	return typ
}

func getJsonName(field *descriptor.FieldDescriptorProto) string {
	options := getFieldOptionCustom(field)
	if strings.Contains(options, "json:") {
		tags, err := structtag.Parse(options)
		if err != nil {
			panic(err)
		}

		jsonTag, err := tags.Get("json")
		if err != nil {
			panic(err)
		}

		if jsonTag.Name == "-" {
			return jsonTag.Name
		}
	}

	return lowercaseFirst(*field.Name)
}

func getTsSocketPath() string {
	socketPath := "@silenteer/hermes/websocket"
	customPath := os.Getenv("TS_SOCKET_PATH")
	if len(customPath) > 0 {
		socketPath = customPath
	}
	return socketPath
}

func getTsMessageExtendFromType(m *descriptor.DescriptorProto) string {
	if opts := m.GetOptions(); opts != nil {
		if e, err := proto.GetExtension(opts, goff.E_ExtendFromType); err == nil {
			return *(e.(*string))
		}
	}
	return ""
}
