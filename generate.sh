#!/usr/bin/env bash
set -e
clear
export DB_PREFIX="tutum_"
export DB_SUFFIX="_db"
go install ./protoc-gen-goff

rm -rf ./out/gitlab.com/silenteer-oss/goff/domains-service
rm -rf ./out/gitlab.com/silenteer-oss/goff/todo-bff
mkdir -p ./out
rm -rf ./out/*/ || true

protoc --proto_path=./ --proto_path=./proto --goff_out ./out ./proto/task.service.proto
protoc --proto_path=./ --proto_path=./proto --goff_out ./out ./proto/task_origin.service.proto

protoc --proto_path=./ --proto_path=./proto --goff_out ./out ./proto/todo.service.proto
protoc --proto_path=./ --proto_path=./proto --goff_out ./out ./proto/todo.bff.proto

protoc --proto_path=./ --proto_path=./proto --goff_out ./out ./proto/todo.common.proto
protoc --proto_path=./ --proto_path=./proto --goff_out ./out ./proto/repo/todo/todo.repo.proto