module gitlab.com/silenteer-oss/goff

go 1.16

require (
	emperror.dev/errors v0.7.0
	github.com/emicklei/proto v1.9.0
	github.com/fatih/structtag v1.2.0
	github.com/getkin/kin-openapi v0.8.0
	github.com/golang/protobuf v1.4.2
	github.com/google/uuid v1.1.1
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	github.com/pelletier/go-toml v1.7.0 // indirect
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.7.1
	gitlab.com/silenteer-oss/titan v1.0.69
	google.golang.org/protobuf v1.25.0
)
