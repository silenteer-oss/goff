package main

import (
	"fmt"
	"strconv"
	"strings"

	"emperror.dev/errors"
	"github.com/emicklei/proto"
)

type Policy struct {
	Group string
	Read  string
	Write string
	Field string
}

type Visitor struct {
	Model          bool
	CollectionName string
	IsTenant       bool
	Errs           []error
	Message        string
	Pkg            string
	Policies       []Policy
}

type Permission struct {
	Field string
	Read  string
	Write string
}

const (
	SYADMIN      = "SYADMIN"
	CAREPROVIDER = "CAREPROVIDER"
	CARECONSUMER = "CARECONSUMER"
	ANONYMOUS    = "ANONYMOUS"
	READ         = "READ"
	WRITE        = "WRITE"
)

var groups = []string{SYADMIN, CAREPROVIDER, CARECONSUMER, ANONYMOUS}
var permissions = []string{READ, WRITE}

func parsePolicy(value string) (Policy, error) {
	policy := Policy{}

	parts := strings.Split(value, ":")

	if len(parts) != 2 {
		return policy, errors.New(fmt.Sprintf("Invalid policy format '%s'", value))
	}

	group, err := parseGroup(parts[0])
	if err != nil {
		return policy, err
	}
	policy.Group = group

	permission, err := parsePermission(group, parts[1])
	if err != nil {
		return policy, err
	}

	policy.Write = permission.Write
	policy.Read = permission.Read
	policy.Field = permission.Field

	return policy, nil
}

func parseGroup(name string) (string, error) {
	group := strings.ToUpper(strings.TrimSpace(name))
	if !IsExist(groups, group) {
		return "", errors.New(fmt.Sprintf("Invalid group name '%s'", name))
	}
	return group, nil
}

func parsePermission(group, expression string) (Permission, error) {
	permission := Permission{}
	var identifyField = ""

	for _, rule := range strings.Split(expression, ",") {
		parts := strings.Split(rule, "=")
		if len(parts) != 2 {
			return permission, errors.New(fmt.Sprintf("Invalid permission '%s'", rule))
		}

		left := strings.ToUpper(strings.TrimSpace(parts[0]))
		right := strings.ToUpper(strings.TrimSpace(parts[1]))

		if !IsExist(permissions, left) {
			return permission, errors.New(fmt.Sprintf("Invalid permission '%s'", rule))
		}

		if left == READ {
			permission.Read = right
		}
		if left == WRITE {
			permission.Write = right
		}

		if right != "~" && right != "*" {
			identifyField = right
		}
	}

	// set default field
	if identifyField == "" && (permission.Read == "~" || permission.Write == "~") {
		switch group {
		case CAREPROVIDER:
			identifyField = "careProviderId"
		case CARECONSUMER:
			identifyField = "accountId"
		}
	}

	permission.Field = identifyField
	return permission, nil
}

func (v *Visitor) VisitMessage(m *proto.Message) {
	if !v.Model {
		v.Message = m.Name
		v.CollectionName = ""
		v.IsTenant = false
		v.Errs = []error{}
	}
}
func (v *Visitor) VisitService(sv *proto.Service) {
}
func (v *Visitor) VisitSyntax(s *proto.Syntax) {
}
func (v *Visitor) VisitPackage(p *proto.Package) {
}
func (v *Visitor) VisitOption(o *proto.Option) {
	name := strings.TrimSpace(o.Name)
	source := strings.TrimSpace(o.Constant.Source)

	if strings.EqualFold(name, "(goff.model)") {
		model, err := strconv.ParseBool(source)
		if err != nil {
			v.Errs = append(v.Errs, errors.WithMessage(err, "parsing Model error "))
		} else {
			v.Model = model
		}
	}

	if strings.EqualFold(name, "(goff.collection_name)") {
		v.CollectionName = source
	}

	if strings.EqualFold(name, "(goff.is_tenant)") {
		isTenant, err := strconv.ParseBool(source)
		if err != nil {
			v.Errs = append(v.Errs, errors.WithMessage(err, "parsing is tenant error "))
		} else {
			v.IsTenant = isTenant
		}
	}

	if strings.EqualFold(name, "go_package") {
		v.Pkg = o.Constant.Source
	}

	if strings.EqualFold(name, "(goff.policy)") && source != "" {
		policy, err := parsePolicy(source)
		if err != nil {
			v.Errs = append(v.Errs, err)
		} else {
			v.Policies = append(v.Policies, policy)
		}
	}

}
func (v *Visitor) VisitImport(i *proto.Import) {
}
func (v *Visitor) VisitNormalField(i *proto.NormalField) {
}
func (v *Visitor) VisitEnumField(i *proto.EnumField) {
}

func (v *Visitor) VisitEnum(e *proto.Enum) {
}
func (v *Visitor) VisitComment(e *proto.Comment) {
}
func (v *Visitor) VisitOneof(o *proto.Oneof) {
}
func (v *Visitor) VisitOneofField(o *proto.OneOfField) {
}
func (v *Visitor) VisitReserved(r *proto.Reserved) {
}
func (v *Visitor) VisitRPC(r *proto.RPC) {
}

func (v *Visitor) VisitMapField(f *proto.MapField) {
}

// proto2
func (v *Visitor) VisitGroup(g *proto.Group)           {}
func (v *Visitor) VisitExtensions(e *proto.Extensions) {}

// implement sort interface
type ByCollectionName []*Visitor

func (a ByCollectionName) Len() int {
	return len(a)
}

func (a ByCollectionName) Less(i, j int) bool {
	return a[i].CollectionName < a[j].CollectionName
}

func (a ByCollectionName) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func IsExist(slice []string, val string) bool {
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}
