package main

import (
	"bytes"
	"flag"
	"fmt"
	"go/format"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/template"
	"unicode"

	"gitlab.com/silenteer-oss/titan"

	path_util "gitlab.com/silenteer-oss/goff/protoc-gen-goff/pkg/path"

	"github.com/iancoleman/strcase"

	"emperror.dev/errors"

	_ "embed"

	"github.com/emicklei/proto"
)

func uppercaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToUpper(v)) + str[i+1:]
	}
	return ""
}

func lowercaseFirst(str string) string {
	for i, v := range str {
		return string(unicode.ToLower(v)) + str[i+1:]
	}
	return ""
}

func toCamel(value string) string {
	return strcase.ToCamel(value)
}

var funcMap = template.FuncMap{
	"UppercaseFirst": uppercaseFirst,
	"LowercaseFirst": lowercaseFirst,
	"ToCamel":        toCamel,
	"ToSnake": func(str string) string {
		return strcase.ToSnake(str)
	},

	"GetPackageName": func(goPackage string) string {
		return goPackage[strings.LastIndex(goPackage, "/")+1:]
	},

	"GetDatabaseName": func(repoName string) string {
		return fmt.Sprintf("%s%s%s", os.Getenv("DB_PREFIX"), repoName, os.Getenv("DB_SUFFIX"))
	},

	"ToLowerCamel": func(str string) string {
		return strcase.ToLowerCamel(str)
	},

	"ToAuthzGroup": func(group string) string {
		authzGroup := ""
		switch group {
		case "SYADMIN":
			authzGroup = "SYS_ADMIN"
		case "CAREPROVIDER":
			authzGroup = "CARE_PROVIDER"
		case "CARECONSUMER":
			authzGroup = "CARE_CONSUMER"
		case "ANONYMOUS":
			authzGroup = "ANONYMOUS"
		}
		return authzGroup
	},

	"ToAuthzReadPermission": func(permission string) string {
		authzPermission := ""
		switch permission {
		case "*":
			authzPermission = "READ_ALL_DATA"
		case "~":
			authzPermission = "READ_SELF_DATA"
		}
		return authzPermission
	},
	"ToAuthzWritePermission": func(permission string) string {
		authzPermission := ""
		switch permission {
		case "*":
			authzPermission = "WRITE_ALL_DATA"
		case "~":
			authzPermission = "WRITE_SELF_DATA"
		}
		return authzPermission
	},
}

type dbconfig struct {
	Data       map[string]map[string][]*Visitor // database name -> feature group  -> collection
	ConfigPkg  string
	FixturePkg string
}

//go:embed config.go.tmpl
var goConfigFile string

//go:embed fixture.go.tmpl
var goFixtureFile string

var logger = titan.GetLogger()

func main() {
	log.SetPrefix("protoc-gen-dbconfig: ")
	var (
		repoDir string

		configOutputFile  string
		configPackageName string

		fixtureOutputFile  string
		fixturePackageName string
	)
	flag.StringVar(&configPackageName, "cp", "client", "Please specify Go config package name")
	flag.StringVar(&configOutputFile, "co", "", "Please specify where to output Go config file")

	flag.StringVar(&fixturePackageName, "fp", "fixture", "Please specify Go fixture package name")
	flag.StringVar(&fixtureOutputFile, "fo", "", "Please specify where to output Go  fixture file")

	flag.Parse()

	if flag.NArg() < 1 {
		logger.Error("Missing repository directory")
		os.Exit(1)
	}

	repoDir = flag.Arg(0)

	logger.Info(repoDir)

	dbcfg, err := parseProtoFiles(repoDir)

	if err != nil {
		logger.Error(fmt.Sprintf("Parsing proto files error: %+v", err))
		os.Exit(1)
	}

	//sort collection mame
	sortedConfig := map[string]map[string][]*Visitor{}
	for dbname, db := range dbcfg {
		sortedSubDb := map[string][]*Visitor{}
		for subName, visitors := range db {
			sort.Sort(ByCollectionName(visitors))
			sortedSubDb[subName] = visitors
		}
		sortedConfig[dbname] = sortedSubDb
	}

	// now generate template
	config := dbconfig{
		Data:       sortedConfig,
		ConfigPkg:  configPackageName,
		FixturePkg: fixturePackageName,
	}

	if configOutputFile != "" {
		err := generateCode(goConfigFile, config, configOutputFile)
		if err != nil {
			logger.Error(fmt.Sprintf("%+v", err))
		}
	}

	if fixtureOutputFile != "" {
		err := generateCode(goFixtureFile, config, fixtureOutputFile)
		if err != nil {
			logger.Error(fmt.Sprintf("%+v", err))
		}
	}

	logger.Info("Done.")
}

func parseProtoFiles(repoDir string) (map[string]map[string][]*Visitor, error) {
	dbcfg := map[string]map[string][]*Visitor{}

	err := filepath.Walk(repoDir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return errors.WithMessage(err, fmt.Sprintf("loading file error  '%s'", path))
			}

			if info.IsDir() || !strings.HasSuffix(path, ".proto") {
				return nil
			}

			if path == "" || path == "." || path == ".." {
				return nil
			}

			fmt.Println(path, info.Size())

			reader, _ := os.Open(path)
			defer reader.Close()

			parser := proto.NewParser(reader)
			definition, err := parser.Parse()
			if err != nil {
				return errors.WithMessagef(err, "parse file error  '%s'", path)
			}

			dbName, subName := path_util.ExtractDbNameFromRepoPath(path)

			visitor := &Visitor{}

			proto.Walk(definition,
				proto.WithService(func(service *proto.Service) {

				}),
				proto.WithOption(func(option *proto.Option) {
					option.Accept(visitor)
				}),
				proto.WithMessage(func(m *proto.Message) {
					m.Accept(visitor)
				}),
			)

			if len(visitor.Errs) > 0 {
				messages := make([]string, len(visitor.Errs))
				for i := 0; i < len(visitor.Errs); i++ {
					messages[i] = visitor.Errs[i].Error()
				}
				return errors.WithMessage(errors.New(strings.Join(messages, "\n")), fmt.Sprintf("visit file error  '%s'", path))
			}

			if visitor.IsTenant && len(visitor.Policies) > 0 {
				return errors.New(fmt.Sprintf("both new Policy and old 'is_tenant' cannot be exist on the same collection, only one method can be choosed '%s'", path))
			}

			if !visitor.Model {
				return nil
			}

			if visitor.CollectionName == "" {
				return errors.Errorf("missing option 'goff.collection_name' in Message '%s', file '%s'", visitor.Message, path)
			}

			db, ok := dbcfg[dbName]
			if !ok {
				db = map[string][]*Visitor{}
			}

			subDb, ok := db[subName]
			if !ok {
				subDb = []*Visitor{}
			}
			subDb = append(subDb, visitor)
			db[subName] = subDb

			dbcfg[dbName] = db

			return nil
		})

	if err != nil {
		return nil, err
	}

	return dbcfg, nil
}

func generateCode(templateFile string, config dbconfig, outputFile string) error {
	goTmpl, err := template.New(".").Funcs(funcMap).Parse(templateFile)
	if err != nil {
		return errors.WithMessagef(err, "parsing template file '%s' error ", templateFile)
	}

	var goBuf bytes.Buffer
	if err := goTmpl.Execute(&goBuf, config); err != nil {
		return errors.WithMessagef(err, "executing template file '%s' error ", templateFile)
	}
	if formattedGoBuf, err := format.Source(goBuf.Bytes()); err != nil {
		return errors.WithMessagef(err, "error: failed to format generated Go code %v", err)
	} else {
		goBuf = *bytes.NewBuffer(formattedGoBuf)
	}

	if outputFile != "" {
		err = ioutil.WriteFile(outputFile, goBuf.Bytes(), 0777)
		if err != nil {
			return errors.WithMessagef(err, "writing to output file '%s' error ", outputFile)
		}
	} else {
		fmt.Println(goBuf.String())
	}

	return nil
}
